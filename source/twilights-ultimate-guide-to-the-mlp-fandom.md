# Twilight's Ultimate Guide to the MLP Fandom

Hello there! For whatever reason, it seems you've stumbled upon this humble guide to the My Little Pony fandom. Perhaps you're a new brony looking to see what the fandom has to offer, or maybe you're an outsider coming to see what all the fuss is about. Or perhaps you're already an experienced member of the fandom, just looking to see if there's anything important you might have missed. Whatever your reason for reading this might be, I hope you'll find this guide enlightening.

A good chunk of this guide is basically just a list of what I consider to be the best original animations, songs, comics, artwork, and stories created by the fandom. It will almost certainly be missing some things, as I definitely can't claim to have seen _everything_ the fandom has to offer, but I'm confident I've covered a good chunk of the more popular works.

Keep in mind that **unless otherwise stated, the links posted here make no attempt to avoid spoilers for new members of the fandom**. The list itself should be reasonably spoiler-free, but click on one of these links without having seen every season of MLP and all the movies and all bets are off (again, unless otherwise stated). That means if you're viewing this list on Reddit using RES and you're concerned about spoilers, make sure none of the below links are expanded before proceeding. Feel free to check with another member of the brony community if you want to be sure that a particular fan work is spoiler-free before you view it. Anyway, read on for a massive list of some of the greatest works in the MLP fandom!


## Animations

The MLP fandom has a number of very high quality (sometimes even as good or better than that of the original show) animations depicting all sorts of things, including original stories, crossovers with video games, interpretations of events from the show, and more.

* [Fall of the Crystal Empire](https://www.youtube.com/watch?v=vL4q7BBzanI)
* [Snowdrop](https://www.youtube.com/watch?v=do6RDSwaWek)
* [Double Rainboom](https://www.youtube.com/watch?v=FKwldMTmLcY)
* [Turnabout Storm](https://www.youtube.com/watch?v=yUDfoZGhLjE&list=PL347AD9B9E509804A)
* [Button Mash Adventures](https://www.youtube.com/watch?v=uFe2MckUAC0)
    * [Deleted Scene (Hearts and Hooves Day)](https://www.youtube.com/watch?v=fWgrmYPplfI)
    * [Bar Buddies](https://www.youtube.com/watch?v=Pi4xYFj-xu4)
* [Remembrance](https://www.youtube.com/watch?v=HZDhMKk4tfg)
* [Once Upon a Time in Canterlot](https://www.youtube.com/watch?v=LFjEintbDXc)
* [Guardian](https://www.youtube.com/watch?v=WRiPItZaue4)
* [My Little Dashie - The Mini Movie](https://www.youtube.com/watch?v=PiBohDujLPs)
* Dinky's Destiny
    * [Prologue](https://www.youtube.com/watch?v=HtMrOOtYExs)
* [Vinyl and Octavia - Sunrise Surprise](https://www.youtube.com/watch?v=AHyw2rqNop0)
* [Just be me](https://www.youtube.com/watch?v=ssbRzCt0n6M)
* [Apple Thief](https://www.youtube.com/watch?v=CFVoJQz4a_4)
* [Duel of the Fates](https://www.youtube.com/playlist?list=PLrProR0wtHPbZpcbpa8oXAHHbiv2ElF8V)
* [Derpy's toy](https://www.youtube.com/watch?v=G0Z0JHb7zGU)
* [Different view of reality](https://www.youtube.com/watch?v=_JGRXSPRMmM)
* [The Cutie Re-Mark Prequel](https://www.youtube.com/watch?v=ukw7Ig0tFTE)
* [You must remember](https://www.youtube.com/watch?v=lDkazAHrYto)
* [Lyra's gift](https://www.youtube.com/watch?v=sD-qWX_E_yE)
* [A Day in Ponyville](https://www.youtube.com/watch?v=mCaHGPdHHCU)
* [Photo Party](https://www.youtube.com/watch?v=tnBWxYT3Uyk)
* [Spies among us](https://www.youtube.com/watch?v=vVU1Jt9BbKA)
* [In Your Dream](https://www.youtube.com/watch?v=UHBPRzWIbx8)
* [Ballad of Princess Platinum](https://www.youtube.com/watch?v=2JmRqWG7Ccc) (Russian with English subs)
* [My Little Pony in the Sims](https://www.youtube.com/playlist?list=PLTOKIqEQT-P6_xhiXD8hebAinl6TiHpQh)
* [My Little Pony: Dota 2](https://www.youtube.com/watch?v=3E5FFFYRUgI)
* [Age of Empires II - My Little Classics, Gaming is Magic](https://www.youtube.com/watch?v=5Qkf4HQ6zQc)


## PMVs

Often animators will set their animations to music. Sometimes music from outside the fandom, and sometimes original music from brony musicians inside the fandom. These animated music videos are referred to by those within the fandom as PMVs, Pony Music Videos.

### Fully-Animated PMVs

These PMVs are composed primarily of original animation from the MLP fandom. There is some overlap here with the "animated" category above, but I tried to put all the videos where the music is a large part of the focus of the animation in this category.

* [Lullaby for a Princess](https://www.youtube.com/watch?v=i7PQ9IO-7fU)
* [The Moon Rises](https://www.youtube.com/watch?v=g2PCNKlddJY)
* [Don't Mine at Night](https://www.youtube.com/watch?v=X_TMtgjQuZI)
* [Children of the Night](https://www.dailymotion.com/video/x282uju)
* [Daylight's End](https://www.youtube.com/watch?v=SLMvyX5yqXI)
* [Luna's Determination](https://www.youtube.com/watch?v=xrngcNy_tnw)
* [A Tale of One Shadow](https://www.youtube.com/watch?v=yDNTNXtsN1Y)
* [Nightmare Night](https://www.youtube.com/watch?v=kle9DkkwlM8)
    * [Nightmare Night: Source Film Maker](https://www.youtube.com/watch?v=MRhRaWNaSPM)
* [Scratch21 - Strangers](https://www.youtube.com/watch?v=b1rvLsXpOH4)
* [A Tropical Octav3](https://www.youtube.com/watch?v=MLeicQZKuKw)
* [Beat It](https://www.youtube.com/watch?v=gmJ2Bkx2C5A)
* ["Spare" animation](https://www.youtube.com/watch?v=pepWxrTh1O8)
* [Come Alive Music Video](https://www.youtube.com/watch?v=WvtULD_qttE)
* [The Count of Monte Cristo Musical](https://www.youtube.com/playlist?list=PLZ2lamdAZ20A5AW-fEz2-Nk4K_a0fCxSw)
* [Catch a Falling Star](https://www.youtube.com/watch?v=KCQ61hW-Gk4)
* [Daddy Discord](https://www.youtube.com/watch?v=xA9dFZJtQWc)
* [Spitfire's Day Off](https://www.youtube.com/watch?v=pp0u7vHLqTs)
* [Frailty ANIMATIC](https://www.youtube.com/watch?v=TA7Vw_vXOVY)

### Mashups & Typography

These are PMVs composed using simple motion graphics and/or carefully selected clips and screenshots from the show or other fan-works. These PMVs are much more common, as they're easier to produce than fully-animated PMVs. Here are a few select stand-outs.

* [The Mane Cast Trailer Series](https://www.youtube.com/playlist?list=PLjEwdNHiqcMTzYxHQEM7KqK5q-raTPa-W)
* [Are we there yet?](https://www.youtube.com/watch?v=pu6kIfaNd6E)
* [Giggles & Gumdrops](https://www.youtube.com/watch?v=LfJIggIySiM)
* [Angel of Darkness](https://www.youtube.com/watch?v=cgRkOlLSfgc)
* [The Stars Will Aid In Her Escape (Cosmic Love)](https://www.youtube.com/watch?v=rV_0n5gp1fE)
* [Summer Sun Celebration](https://www.youtube.com/watch?v=PFNtwSUX3ls)



## Music

As I mentioned in the previous section, there are a number of artists in the MLP fandom who create original songs inspired by the show. Some of these are remixes of songs from the show, while others are entirely original.

There's lots of pretty amazing stuff here; definitely more than I can cover in this post, so here's a sample of some of my favorites. (I tried my best to cram these into just a few major categories, so sorry if the genres don't fit perfectly in all cases here. I'm open to suggestions if you've got any ideas on how to categorize these better.)

### Pop (sort of)

* [Faster Than You Know](https://www.youtube.com/watch?v=8JJg9114bgs)
* [Friendship](https://www.youtube.com/watch?v=9U0UUDy_5ik)
* [Crusader (Are We There Yet)](https://www.youtube.com/watch?v=K4bBNbiVrus)
* [Double Rainboom](https://www.youtube.com/watch?v=J5bhT4-9M0o)
* [Set in Stone](https://www.youtube.com/watch?v=j-hM3q5T1H4)
* [She's a Pony](https://www.youtube.com/watch?v=t38DPnkDjTM)
* [Open Your Eyes](https://www.youtube.com/watch?v=ia2aoTYY9PI)
* [Good Girl](https://www.youtube.com/watch?v=q1pjmN3RF3c)
* [Until the Sun](https://www.youtube.com/watch?v=pOHvWX8c6Vs)
* [Wings](https://www.youtube.com/watch?v=Huh8EHg6X4M)
* [Giggles & Gumdrops](https://www.youtube.com/watch?v=52cuuWHNWIU)
* [Anthropology (Lyra's Song)](https://www.youtube.com/watch?v=S4dWMxTWTXE)
* [Nightmare Night](https://www.youtube.com/watch?v=9PCEp8z7FNg)
* [Discord](https://www.youtube.com/watch?v=xPfMb50dsOk)
  * [The Original](https://www.youtube.com/watch?v=jDMGv3hNMes)
* [Flowers](https://www.youtube.com/watch?v=aLC8Lc3iu-k)

### Metal

* [Sonic Rainboom](https://www.youtube.com/watch?v=uCA6PjX0zm8)
* [Loyalty](https://www.youtube.com/watch?v=voj9MhBUaTI)
* [Fall of an Empire - Royal Canterlot Symphonic Metal Orchestra](https://www.youtube.com/watch?v=vtBqTFsiLxM)
* [Moonrise: A Symphonic Metal Opera](https://www.youtube.com/watch?v=IBvl-AdnHGY)
* [Frailty (Ft. Milkymomo)](https://www.youtube.com/watch?v=w_aNxAIndik)
* [Colored Lights (Extended Version)](https://www.youtube.com/watch?v=F1qcewO63xQ)
* [Constellations (Ft. Dreamchan)](https://www.youtube.com/watch?v=UpPiReylIu0)

### Mashups & Remixes

* [MLP:FiM Season 1-4 Medley](https://www.youtube.com/watch?v=9tCe_WjIbQw)
* [Together](https://www.youtube.com/watch?v=hn3Pa1Z6JjE)
* [I'll Fly (Sim Gretina Remix)](https://www.youtube.com/watch?v=pjtnYHp1p7M)
* [Behold](https://www.youtube.com/watch?v=Q-xjk5X0cs8)

### Classical

* [Lullaby for a Princess](https://www.youtube.com/watch?v=H4tyvJJzSDk)
    * [Luna's Reply](https://www.youtube.com/watch?v=waPvh7ehZ3Y)
* [Harmony Ascendant](https://www.youtube.com/watch?v=82prN2pF9Zo)
* [My Cadence](https://www.youtube.com/watch?v=YzSl6LpXF1s)
* [The Moon Rises](https://www.youtube.com/watch?v=kPjVCIX5Fvs)
* [A Nightmares Melody](https://www.youtube.com/watch?v=9RPZCQgecTY)

### Instrumentals

* [[Redacted] Twilight Orchestral Suite](https://www.youtube.com/watch?v=ulCF77j_kS0)
* [Friendship is Magic (Orchestral Arrangement)](https://www.youtube.com/watch?v=RtHJ8lFUrWI)
* [Pony Empires](https://www.youtube.com/playlist?list=PLOP4OcXrhLdz_wm1YRngfUA8gFMQkGA5V)
* [Love Conquers All](https://www.youtube.com/watch?v=0ViDPWCcwmg)


## Musical artists

Here are a few of my favorite musical artists in the fandom, some of whom are responsible for the songs mentioned in the section above.

* [Black Gryph0n](https://www.youtube.com/user/christkids)
* [ponyphonic](https://www.youtube.com/user/ponyphonic)
* [Aviators](https://www.youtube.com/user/SoundOfTheAviators)
* [MandoPony](https://www.youtube.com/user/MandoPony)
* [Pony Archie](https://www.youtube.com/user/buckallponyhaters69)
* [Foozogz](https://www.youtube.com/user/PepperBrony)
* [MelodicPony](https://www.youtube.com/user/MelodicPony)
* [The Living Tombstone](https://www.youtube.com/channel/UCFYMDSW-BzeYZKFSxROw3Rg)
* [WoodenToaster](https://www.youtube.com/user/WoodenToaster)
* [Evening Star](https://www.youtube.com/user/PonyEveningStar)
* [SlyphStorm](https://www.youtube.com/channel/UCLGfw6f5oWW8aRKYQ8SVjgw)
* [4everfreebrony](https://www.youtube.com/channel/UCc1DS7Is5iUZyACnhtKMMiw)
* [Artem Yegorov](https://www.youtube.com/user/MDexart)
* [SoGreatandPowerful](http://sogreatandpowerful.com/)
* [WeimTime](https://www.youtube.com/user/WeimTime007)
* [FritzyBeat](https://www.youtube.com/user/FritzyBeat)
* [Jyc Row](https://www.youtube.com/user/Zancrowsama)
* [PrinceWhateverer](https://www.youtube.com/user/princewhateverer)
* [WoodLore](https://www.youtube.com/user/WoodLoreMLP)
* [Aurelleah](https://www.youtube.com/user/DorianStorm2)
* [Matías Peñaloza](https://www.youtube.com/user/matias7penaloza)


## Analysts & Theorycrafters

Some members of the fandom find it fun to critique episodes of the show, or to spend time coming up with theories and headcanons explaining details about the world of Equestria which are not fully explored in the show itself. Here are a few of the more popular ones (including a small sample of some of their best works).

* [The Brony Notion](https://www.youtube.com/channel/UCD_VOth7RmckN6DbmFJa__A)
    * [Analyzing Star Swirl's Spell](https://www.youtube.com/watch?v=UUUnrFPBxkU)
    * [Does Twilight have OCD?](https://www.youtube.com/watch?v=Xp0RtsHZ1E4)
    * [Spike's Dream in Winter Wrap Up](https://www.youtube.com/watch?v=oyIER1JZ318)
    * [How Does the Rainboom Work?](https://www.youtube.com/watch?v=YwybXOyc2MA)
* [DRWolf001](https://www.youtube.com/user/DRWolf001)
    * [Is Celestia's Power Weakening?](https://www.youtube.com/watch?v=NXXD1yQA12k)
    * [Analysing Mental Breaks in MLP](https://www.youtube.com/watch?v=zz6wBIiXYGo)
    * [Fluttershy's weakness is part of her greatest strength](https://www.youtube.com/watch?v=0NP45dyp2dQ)
    * [Does Pinkie Pie have The Most Important job in Ponyville?](https://www.youtube.com/watch?v=zBDMY-V4_68)
* [LittleshyFiM](https://www.youtube.com/user/LittleshyFiM) - CinemaSins, but with Ponies
* [Silver Quill](https://www.youtube.com/user/tgcsilver)
    * [After the Fact: Flash Sentry](https://www.youtube.com/watch?v=A1uk-iYyR4w)
    * [After the Fact: Amending Fences](https://www.youtube.com/watch?v=JlkcyjXj1e8)
* [ILoveKimPossibleAlot](https://www.youtube.com/user/ILoveKimPossibleAlot)
* [Ink Rose](https://www.youtube.com/user/AhsokaAnya)


## Blind Commentators

Want to relive the joy of seeing the show for the first time? Want to hear what others thought of an episode? A number of people have recorded themselves watching MLP (and related fan works) for the first time (i.e. "blind"), and sharing their thoughts with you as they watch.

* [Bronies React](https://www.youtube.com/playlist?list=PL7A322D867992908B) (Not always blind, but super-popular.)
* [xXSoundspeedXx](https://www.youtube.com/user/xXSoundspeedXx)
* [MasterNeutral1](https://www.youtube.com/user/MasterNeutral1)
* [MrCobaltSky](https://www.youtube.com/user/MrCobaltSky)
* [Equestria's Reflection](https://www.youtube.com/user/EquestriasReflection)
* [Luffyiscool](https://www.youtube.com/user/luffyiscool)
* [CoHNaturesFury](https://www.youtube.com/user/CoHNaturesFury)
* [LaDix](https://www.youtube.com/user/LaDixCommentaries)
* [BronyBurningAxe](https://www.youtube.com/channel/UCbMRE97faCqty4SIknIsEXw)
* [JustABritishGuy](https://www.youtube.com/channel/UCrifuBxhrGfrhQMQXu2llqQ)


## Art

There's really so much great content in this category that I can't even begin to compile a comprehensive list of "the best". Below is a small sample of artwork the fandom has created. Beyond that, I suggest you check out [what's on DeviantArt](http://best-of-mlp-fim.deviantart.com/gallery/) and branch out from there. I also have my own collections of my favorite artwork of [canon](https://ajedi32.deviantart.com/favourites/63069176/Ponies) and [non-canon](https://ajedi32.deviantart.com/favourites/71416469/Pony-OCs) characters on DeviantArt which you're free to check out.

* [My Little Pony: Friendship is Magic](https://viwrastupr.deviantart.com/art/My-Little-Pony-Friendship-is-Magic-667085058) by viwrastupr
* [Meh](https://ponykillerx.deviantart.com/art/Meh-370390567) by ponyKillerX
* [[Redacted] Cadance](https://sugarberryart.deviantart.com/art/Princess-Cadance-657830253) by SugarberryArt
* [Airwaves](http://blitzpony.deviantart.com/art/Airwaves-341712006) by BlitzPony
* [Selfie before sunset](https://gianghanez2880.deviantart.com/art/Selfie-before-sunset-659171068) by GiangHanez2880
* [Out of the Ashes, a New Story To Be Told](http://huussii.deviantart.com/art/Out-of-the-Ashes-a-New-Story-To-Be-Told-454909657) by Huussii
* [Imperialism](https://yakovlev-vad.deviantart.com/art/Imperialism-524695294) by Yakovlev-vad
* [levitation_ Sketch](https://magnaluna.deviantart.com/art/levitation-Sketch-655618609) by MagnaLuna
* [Dash](http://ankard.deviantart.com/art/Dash-333932281) by Ankard
* [Lunar Serenity](https://viwrastupr.deviantart.com/art/Lunar-Serenity-551296711) by viwrastupr


## Comics

Some of the artists in the community have used their skills to create comics about the show. Some of these are random silliness and gags, while others feature original stories.

### Character Blogs

These are basically blogs where the featured characters respond to questions from the readers. Try a few, you'll get the idea.

* [Horse Wife](http://thehorsewife.tumblr.com/)
* [Ask the Cutie Mark Crusaders](http://ask-thecrusaders.tumblr.com/)
* [Ask Fluffle Puff](http://askflufflepuff.tumblr.com/tagged/mlp+fim/chrono)
* [Dan Vs. FiM](http://dan-vs-fim.tumblr.com/)
* [Ask Teen Chrysalis](http://ask-teen-chrysalis.tumblr.com/)

### Longform Story Comics

* [Friendship is Magic](http://mauroz.deviantart.com/art/Friendship-Is-Magic-introduction-330419485)
* [Recall the Time of No Return](http://gashiboka.deviantart.com/art/Recall-the-Time-of-No-Return-page-link-library-541068613)
* [Dash Academy](http://dashacademycomic.com/)
* [Lunar Isolation](https://thedracojayproduct.deviantart.com/art/Lunar-Isolation-START-HERE-491750348)
* [Friendship is Dragons](http://friendshipisdragons.thecomicseries.com)

### Shorts

* [The Reunion](http://imdrunkontea.deviantart.com/art/The-Reunion-1-3-570963338)
* [Art Block](http://mister-saugrenu.deviantart.com/art/Art-Block-page-1-562631838)
* [Doesn't Matter](http://dilarus.deviantart.com/art/Doesn-t-Matter-Page-1-637872092)
* [The Royal Flu](https://pony-berserker.deviantart.com/art/The-Royal-Flu-Comic-cover-463749287)
* [Lessons](http://dsana.deviantart.com/art/Lessons-Page-1-643368847)
* [Closed for the Holidays](https://dilarus.deviantart.com/art/Closed-for-the-Holidays-Page-1-652549225)
* [To Look After](https://dsana.deviantart.com/art/To-Look-After-Page-1-651413490)
* [Left Behind](http://dsana.deviantart.com/art/Left-Behind-Page-1-667973809)
* [A Princess' Worth](https://saturdaymorningproj.deviantart.com/art/A-Princess-Worth-Cover-614387046)


## Fanfictions

And of course, like just about every other fandom in existence, MLP has a number of fanfictions written about it. I personally haven't explored this area of the fandom too much, but here are a few works others have frequently recommended to me. For discovering more stories, [FIMFiction.net](http://www.fimfiction.net/) is a good resource, along with some of the reviewer sites and groups out there like [Seattle's Angels](http://www.fimfiction.net/group/1734/seattles-angels) and the [Royal Canterlot Library](http://royalcanterlotlibrary.net/).

* [Fallout: Equestria](http://www.fimfiction.net/story/119190/fallout-equestria) (This one is big enough to actually have its own subculture within the fandom.)
* [Past Sins](http://www.equestriadaily.com/2011/07/story-past-sins.html)
* [Through the Well of Pirene](http://www.fimfiction.net/story/73404/through-the-well-of-pirene)
* [Arrow 18 Mission Logs: Lone Ranger](https://www.fimfiction.net/story/13616/arrow-18-mission-logs-lone-ranger)
* [The Celestia Code](http://www.fimfiction.net/story/141549/the-celestia-code)
* [Sunny Skies All Day Long](http://www.fimfiction.net/story/20685/sunny-skies-all-day-long)
* [Hard Reset](http://www.fimfiction.net/story/67362/hard-reset)
* [The Keepers of Discord](http://www.fimfiction.net/story/25125/the-keepers-of-discord)
* [The Best Night Ever](http://www.fimfiction.net/story/18087/the-best-night-ever)
* [Princess Celestia Hates Tea](http://www.fimfiction.net/story/29271/princess-celestia-hates-tea)
* [My Little Dashie](http://www.fimfiction.net/story/1888/my-little-dashie)
* [Anthropology](http://www.fimfiction.net/story/4656/Anthropology)
* [Vinyl and Octavia: University Days](https://www.fimfiction.net/story/13477/vinyl-and-octavia-university-days)


## Games

A number of creators have teamed up to create full fledged video games based on the show. For discovering more games and mods, right now [Equestria Daily](https://www.equestriadaily.com/search/label/Game) is probably your best bet.

* [Fighting is Magic](http://www.equestriadaily.com/2011/06/pony-fighting-game.html) - Street Fighter style fighting game (never finished)
  * [Them's Fightin' Herds](https://www.indiegogo.com/projects/them-s-fightin-herds) - _Fighting is Magic_ ran into legal trouble and got turned into this
  * [Fighting is Magic Tribute Edition](http://www.equestriadaily.com/2014/02/fighting-is-magic-tribute-edition.html) - New group of developers working on a fork of the original project (no longer being developed)
  * [Fighting is Magic MEGA](http://fightingismagicmega.yolasite.com/) - Another fork of the original project
* [Legends of Equestria](http://www.legendsofequestria.com/) - MMORPG
* [Fallout Equestria: Remains](https://empalu.deviantart.com/art/FoE-Remains-version-0-6-715528494)
* [Friendship is EPIC](http://fie.main.jp/b/en/) - 3D Sidescrolling Co-Op Action/Adventure (In Development)
* [Ambient.White](http://albdifferent.com/ambient-project/) - 3D Adventure/Fantasy Game (In Development)
* [My Little Investigations](http://www.equestriandreamers.com/) - Mystery game based on [Ace Attorney Investigations](https://en.wikipedia.org/wiki/Ace_Attorney_Investigations:_Miles_Edgeworth)
* [Story of the Blanks](http://www.newgrounds.com/portal/view/573755) - Spooky
* [Mega Pony](https://www.youtube.com/watch?v=VwMOOHb1aeA) - Mega Man, but with Ponies
* [PonyTown](https://pony.town/) - Run around as your pony avatar and chat
* [My Little Karaoke](https://www.mylittlekaraoke.com/)
* [PonyVRville](http://kaitouace.com/vr/) - Ponyville + Virtual Reality (Compatible with the Oculus Rift)
* [Twilight Escape](https://timeshield.neocities.org/) - 3D RPG (Spooky)
* [Day Dreaming Derpy](https://rpgmaker.net/games/9424/) - JRPG
* [The Pony Platforming Project](http://www.dragon-mango.com/ponygame/) - Platformer
* [Unicorn Training](https://play.google.com/store/apps/details?id=com.yotesgames.training) - Top-down RPG for Android ($0.99)


### Mods

* **Minecraft**
  * [Mine Little Pony](http://www.minecraftforum.net/forums/mapping-and-modding/minecraft-mods/1278090-mine-little-pony-v1-6-4-1)
  * [The Brony Modpack](http://voxelmodpack.com/modpacks.html)
* **TF2**
  * [MLPTF2Mods.com](http://mlptf2mods.com/)
* **Skyrim**
  * [My Little Pony Mod](http://www.nexusmods.com/skyrim/mods/26677/?)
* **Any other game**
  * Just Google "My Little Pony \<video game name here\>". There's a decent chance it exists.


## Fansites

Trying to find more awesome pony content? Looking to talk with other bronies? There are all sorts of places on the web for that!

* [Equestria Daily](http://www.equestriadaily.com/) - Pretty much the #1 site for news about the show and new fan creations
* [FIMFiction](http://www.fimfiction.net/) - Fanfiction, but with ponies
* [/r/mylittlepony on Reddit](https://www.reddit.com/r/mylittlepony/) - User-submitted news and fan content
* [/r/MLPlounge on Reddit](https://www.reddit.com/r/MLPlounge) - Discussions with other fans
* [Derpibooru](https://derpibooru.org/) - Image board, with an excellent tagging system
* [Friendship is Magic Wiki](http://mlp.wikia.com/wiki/My_Little_Pony_Friendship_is_Magic_Wiki) - Information about the show itself (canon only, no fan content)
* [MLP Fan Labor Wiki](http://mlpfanart.wikia.com/wiki/My_Little_Pony_Fan_Wiki) - Catalog of fan works, and information about MLP [fanon](http://tvtropes.org/pmwiki/pmwiki.php/Main/Fanon).
* [My Little Pony: Friendship is Magic on TV Tropes](http://tvtropes.org/pmwiki/pmwiki.php/WesternAnimation/MyLittlePonyFriendshipIsMagic) - Lists of memes, tropes, and fan content
* [BronyTunes](https://bronytunes.com) - iTunes, but with ponies
* [Ponyville Live](http://ponyvillelive.com/) - Radio stations, livestreams, podcasts, and conventions
* [BronyTV](http://bronytv.net/) - Livestream for new episodes; complete with chat
* [BronyState](http://www.bronystate.net/) - Another site for livestreams for new episodes; also with chat
* [PonyVerse](https://poniverse.net/) - Parent site for...
  * [MLPForums](https://mlpforums.com/) - Exactly what it sounds like
  * [Pony.fm](https://pony.fm/) - Soundcloud, but with Ponies
  * [Equestria.tv](http://equestria.tv/) - Livestreams, including new episodes
  * [Poniarcade](http://poniarcade.com/) - Steam group, game servers, and TeamSpeak
* [Royal Canterlot Library](http://royalcanterlotlibrary.net/) - Fanfiction reviews
* [One Man's Pony Ramblings](http://onemansponyramblings.blogspot.com/) - More fanfiction reviews!
* [The Big Master Review List](https://docs.google.com/spreadsheets/d/18VeLh_RPbljlf948E_MExiJxGboubPwDGJsikagWi3w) - ALL THE FANFICTION reviews


## Parodies

Some animators have created parodies of the original show. Lots of random humor and crazy shenanigans here. If you've ever seen any fan-created "abridged series" of other shows, you should a pretty good idea of what these are.

Note that some of the videos in these series may include strong language and mature humor, so be warned.

* [The Mentally Advanced Series](https://www.youtube.com/playlist?list=PL6109C69F1D59ACD9)
* [PONIES the Anthology](https://www.youtube.com/watch?v=2Tjb14VoWjg)
* [Totally Legit Recaps](https://www.youtube.com/playlist?list=PLufmYJVoXKrBaVNVsHM3p7onpkc0zvFA0)
* [Rainbow Dash Presents](https://www.youtube.com/playlist?list=PL112E2C1E3085C4C7)
* [Friendship is Witchcraft](https://www.youtube.com/playlist?list=PL8cGaJKvM_-5lkA0h_cwWxYrC-fc27apd)
* [Ultra Fast Pony](https://www.youtube.com/playlist?list=PLX113_fWui8qzNrGtCEJIKr7eHEEnCCzT)
* [[HORSE]](https://www.youtube.com/playlist?list=PLhqcyv6oSxRDnMorf8sD6nnf_qK4jyeHQ)


## Memes and Culture

There are a are number of popular memes in the fandom. I don't necessarily consider these to be "top quality" animations or photos (though some of them _are_ pretty funny), but nonetheless they're important to understand in that they give you context for understanding cultural references and inside jokes in the fandom.

* [Love and Tolerate](http://vignette1.wikia.nocookie.net/mylittlebrony/images/3/31/Love_and_Tolerate.png/revision/latest?cb=20111011183118)
* [20% cooler](http://gyropedia.wikia.com/wiki/20_Percent_Cooler)
* [Post Ponies](http://knowyourmeme.com/memes/mods-are-asleep#my-little-pony-threads)
* [Pony shrug](http://gyropedia.wikia.com/wiki/Pony_Shrug) [](/ppshrug-in)
* [Brohoof](http://img3.wikia.nocookie.net/__cb20120310072234/mlp/images/3/3a/Applejack_%26_Rainbow_brohoof_S1E13.png) /)(\
* [Ponifying](http://knowyourmeme.com/memes/ponify)
* [OCs](http://www.urbandictionary.com/define.php?term=OC&defid=3209771)
* [Friendship is Magic B****](https://www.youtube.com/watch?v=k4f9m4OYkCY)
* [Friendship is Manly](https://www.youtube.com/watch?v=-Z7UnO66q9w)
* [Ponies with Hats](https://www.youtube.com/watch?v=OqAkq8v6LuQ) (Parody of [Llamas with Hats](http://www.youtube.com/watch?v=kZUPCB9533Y))
* [Cupcakes](http://mlpfanart.wikia.com/wiki/Cupcakes)
    * Smile HD
* [Twilicane](http://knowyourmeme.com/memes/the-twilight-sparkle-scepter) (Season 4 spoilers)
* [But I didn't listen](http://knowyourmeme.com/memes/but-i-didnt-listen) (Season 5 spoilers)
* [The show itself was originally popularized as an internet meme](http://knowyourmeme.com/memes/subcultures/my-little-pony-friendship-is-magic)


## Background Ponies

Throughout many episodes in the show you'll often see random ponies placed in the background for the purposes of filling out a crowd or livening up the town. They're basically nameless "extras". Except, thanks to the fandom, they're actually not nameless. In fact, most of these random ponies have names and backstories given to them by the MLP fandom. Here are a few of the more well-known background Ponies:

* [Derpy Hooves](http://mlpfanart.wikia.com/wiki/Derpy_Hooves)
* [Doctor Whooves](http://mlpfanart.wikia.com/wiki/Doctor_Whooves)
* [Bon Bon](http://mlpfanart.wikia.com/wiki/Bon_Bon) (aka Sweetie Drops)
* [Lyra Heartstrings](http://mlpfanart.wikia.com/wiki/Lyra)
* [DJ Pon-3](http://mlpfanart.wikia.com/wiki/Vinyl_Scratch) (aka Vinyl Scratch)
* [Octavia Melody](http://mlpfanart.wikia.com/wiki/Octavia)
* [Carrot Top](http://mlpfanart.wikia.com/wiki/Carrot_Top)
* [Amethyst Star](http://mlpfanart.wikia.com/wiki/Sparkler) (aka Sparkler)
* [Minuette](http://mlpfanart.wikia.com/wiki/Colgate) (aka Colgate)
* [Berry Punch](http://mlpfanart.wikia.com/wiki/Berry_Punch)
* And more...


## Official content you might not have seen

Aside from the show itself, Hasbro has released a few original comics, animated shorts, and songs which tie in to the actual show. While these aren't technically fan-created, I figured I'd mention them here anyway just to make sure you didn't miss them.

### Equestria Girls Animated Shorts

#### Rainbow Rocks Prequel Shorts

* [Music to My Ears](https://www.youtube.com/watch?v=qh7j1qUaGOc)
* [Guitar Centered](https://www.youtube.com/watch?v=Fg46129oPIs)
* [Hamstocalypse Now](https://www.youtube.com/watch?v=BniijKHywXM)
* [Pinkie on the One](https://www.youtube.com/watch?v=iTmZ3UEpkBs)
* [Player Piano](https://www.youtube.com/watch?v=hBxfpBquX-o)
* [A Case for the Bass](https://www.youtube.com/watch?v=z7z8TEpYMYs)
* [Shake your Tail!](https://www.youtube.com/watch?v=fdVdpRzUzR0)
* [Perfect Day for Fun](https://www.youtube.com/watch?v=cKBTsZgk1aA)

#### Rainbow Rocks Encore Shorts

* [Life is a Runway](https://www.youtube.com/watch?v=6C7D5Tr5Sj0)
* [My Past is Not Today](https://www.youtube.com/watch?v=XqnbYUG6Bn8)
* [Friendship Through the Ages](https://www.youtube.com/watch?v=-3ixfTKGG8A)

#### Friendship Games Prequel Shorts

* [The Science of Magic](https://www.youtube.com/watch?v=BOdThTzb_tY)
* [Pinkie Spy](https://www.youtube.com/watch?v=f-UZYMKA0nw)
* [All's Fair in Love & Friendship Games](https://www.youtube.com/watch?v=e44a--DJ30c)
* [Photo Finished](https://www.youtube.com/watch?v=sPPL9KV8WXQ)
* [A Banner Day](https://www.youtube.com/watch?v=xkkbsnEw5bw)

#### Frindship Games Deleted Scenes & Bloopers

* [Twilight Spakle and Sunset Shimmer at the Cafe](https://www.youtube.com/watch?v=0Xybq7jItRw)
* [Alternate Hallway Scene](https://www.youtube.com/watch?v=QvX8UGgxlns)
* [Alternate version of _What More is Out There_](http://www.dailymotion.com/video/x3bk178) (Featuring Sunset Shimmer)
* [Alternate Ending](https://www.youtube.com/watch?v=lLmpL0orwlE)
* [Bloopers](https://www.youtube.com/watch?v=3wzPCQyaPA8) (Fully Animated)

#### Legend of Everfree Bloopers

No shorts this time, just bloopers.

* [Bloopers](https://www.youtube.com/watch?v=MjHnhO-_Jmo)

### Comics

* [My Little Pony: Friendship is Magic](http://mlp.wikia.com/wiki/My_Little_Pony:_Friendship_is_Magic_%28comics%29)
* [My Little Pony Micro-Series](http://mlp.wikia.com/wiki/My_Little_Pony_Micro-Series)
* [My Little Pony: Friends Forever](http://mlp.wikia.com/wiki/My_Little_Pony:_Friends_Forever)
* [My Little Pony: FIENDship is Magic](http://mlp.wikia.com/wiki/My_Little_Pony:_FIENDship_is_Magic)
* Annual editions
    * [2013](http://mlp.wikia.com/wiki/My_Little_Pony_Annual_2013)
    * [2014](http://mlp.wikia.com/wiki/My_Little_Pony_Annual_2014)
* Holiday Specials
	* [Equestria Girls Holiday Special](http://mlp.wikia.com/wiki/My_Little_Pony:_Equestria_Girls_Holiday_Special)
	* [Pony Holiday Special](http://mlp.wikia.com/wiki/My_Little_Pony_Holiday_Special)

### Music

* [My Little Pony Theme - Extended Version](https://www.youtube.com/watch?v=ZcBNxuKZyN4)
* [Equestria Girls](http://www.equestriadaily.com/2011/05/extended-equestria-girls.html) - Hub promo video parodying Katy Perry's song, [_California Girls_](https://www.youtube.com/watch?v=F57P9C4SAW4); not related to the Equestria Girls movies
* [My Little Pony Friends](https://www.youtube.com/watch?v=9aJyDsL2wMU) - Alternate end credits song for Equestria Girls
* [True True Friend Winter Wrap-Up (Ultimate Mash-Up)](https://www.youtube.com/watch?v=pXxNdyO7bG8)
* [Dance Magic](https://www.youtube.com/watch?v=Ko6e1K6hDX0)
* [It's a Pony Kind of Christmas](http://mlp.wikia.com/wiki/It's_a_Pony_Kind_of_Christmas) - Pony-themed Christmas album

#### International Opening/Closing Themes

Several versions of the MLP theme song from other countries feature lyrics and tunes completely different from their English counterparts.

* [Italian Season 1 Opening](https://www.youtube.com/watch?v=7FVqFN_Hn20) ([Extended Version](https://www.youtube.com/watch?v=9RbvA7-VMUQ)
* [Japanese Opening 1 (Mirai Start)](http://www.dailymotion.com/video/xzssdd_my-little-pony-japanese-opening-mirai-start_shortfilms)
* [Japanese Opening 2 (Majikaru Dai☆Dai☆Dai-Bōken!)](http://www.dailymotion.com/video/x11qtrp)
* [Japanese Opening 3 (Yumemiru! Shinjiru! Mirai Kanaete!)](http://www.dailymotion.com/video/x19yyrw)
* [Japanese Opening 4 (Lucky Girl)](https://www.youtube.com/watch?v=YuxRSeSqmuc)
* [Japanese Season 1 Closing (Kataomoi no Karaage)](https://www.youtube.com/watch?v=tjBWj_HfEgs)
* [Japanese Season 2+ Closing (Tomodachi wa Mahou, step by step)](https://www.youtube.com/watch?v=sfCM-TXNjKQ) ([Live Action Full Version](https://www.youtube.com/watch?v=7F_eH--rZUU))

### Promos

These are a series of commercials The Hub and Discovery Family ran to advertise the show.

* [Life without Ponies](https://www.youtube.com/watch?v=ZXCLD20a33Y)
* [Equestria Girls](http://www.equestriadaily.com/2011/05/extended-equestria-girls.html) - Actually unrelated to the movie by the same name
* [Hot Minutes With the Mane 6](https://www.youtube.com/playlist?list=PLRT3ZI43j2UN_zGhMBlvUlUqUKC7AYOR9)
* [Dan vs Pinkie Pie](https://www.youtube.com/watch?v=8C0DukozIYs)
* [Fresh Princess of Friendship](https://www.youtube.com/watch?v=BMvwko9jY_I)
* [My Little Pony: Dragonfire (Promo)](https://www.youtube.com/watch?v=S-DDF0q68kw)

The above is a small selection of some of the best commercials. See [here](http://mlp.wikia.com/wiki/Commercials#Show) for a full list.


### Misc. Animated Shorts

* [Happy Birthday to You!](http://mlp.wikia.com/wiki/Happy_Birthday_to_You!)


### Books

There are also a number of storybooks licensed or published by Hasbro. Some contain fully original stories, while others are adaptations of existing MLP movies.

The list below only includes books authored by writers for the actual show. If you include other officially licensed books there's actually [a lot more](http://mlp.wikia.com/wiki/List_of_storybooks), but most of those are targeted towards younger children and aren't particularly interesting...

* [Twilight Sparkle and the Crystal Heart Spell](http://mlp.wikia.com/wiki/Twilight_Sparkle_and_the_Crystal_Heart_Spell)
* [My Little Pony: Pinkie Pie and the Rockin' Ponypalooza Party!](http://mlp.wikia.com/wiki/Pinkie_Pie_and_the_Rockin%27_Ponypalooza_Party!)
* [Equestria Girls: Through the Mirror](http://mlp.wikia.com/wiki/Chapter_books#Through_the_Mirror)
* [Rainbow Dash and the Daring Do Double Dare](http://mlp.wikia.com/wiki/Rainbow_Dash_and_the_Daring_Do_Double_Dare)
* [Rarity and the Curious Case of Charity](http://mlp.wikia.com/wiki/Rarity_and_the_Curious_Case_of_Charity)
* [Journal of the Two Sisters](http://mlp.wikia.com/wiki/The_Journal_of_the_Two_Sisters) - Lots of Lore about Celestia and Luna; definitely recommended
* [Applejack and the Honest-to-Goodness Switcheroo](http://mlp.wikia.com/wiki/Applejack_and_the_Honest-to-Goodness_Switcheroo)
* [Daring Do and the Marked Thief of Marapore](http://mlp.wikia.com/wiki/Daring_Do_and_the_Marked_Thief_of_Marapore)
* [Daring Do and the Eternal Flower](http://mlp.wikia.com/wiki/Daring_Do_and_the_Eternal_Flower)
* [Daring Do and the Forbidden City of Clouds](http://mlp.wikia.com/wiki/Daring_Do_and_the_Forbidden_City_of_Clouds)
* [Fluttershy and the Fine Furry Friends Fair](http://mlp.wikia.com/wiki/Fluttershy_and_the_Fine_Furry_Friends_Fair)
* [Princess Celestia and the Summer of Royal Waves](http://mlp.wikia.com/wiki/Princess_Celestia_and_the_Summer_of_Royal_Waves)
* [Discord and the Ponyville Players Dramarama](http://mlp.wikia.com/wiki/Discord_and_the_Ponyville_Players_Dramarama)
* [Princess Luna and the Festival of the Winter Moon](http://mlp.wikia.com/wiki/Princess_Luna_and_the_Festival_of_the_Winter_Moon)
* [Lyra and Bon Bon and the Mares from S.M.I.L.E.](http://mlp.wikia.com/wiki/Lyra_and_Bon_Bon_and_the_Mares_from_S.M.I.L.E.)
* [[Redacted] Cadance and the Spring Hearts Garden](http://mlp.wikia.com/wiki/Princess_Cadance_and_the_Spring_Hearts_Garden)
* [Twilight's Sparkly Sleepover Surprise](http://mlp.wikia.com/wiki/Twilight%27s_Sparkly_Sleepover_Surprise)
* [Starlight Glimmer and the Secret Suite](http://mlp.wikia.com/wiki/Starlight_Glimmer_and_the_Secret_Suite)

### Game

There's actually an official free-to-play (with microtransactions) MLP mobile game created by Gameloft.

Downloads: [Android](https://play.google.com/store/apps/details?id=com.gameloft.android.ANMP.GloftPOHM) | [iOS](https://itunes.apple.com/us/app/my-little-pony-friendship/id533173905?mt=8)


## Highlights

Now I'd like to highlight a few of my all time favourites from that massive list above. In my opinion, these are works that all MLP fans should definitely watch at some point.

And yes, these are all animations and PMVs. That's mainly because no work in any other category has managed to wow me enough to make it into this list. For awesome works in other categories, the master list above should suffice. Maybe once I start reading more fanfictions I'll find something from that category to put here, but for now it's basically all just animations.

[**_Fall of the Crystal Empire_**](https://www.youtube.com/watch?v=vL4q7BBzanI) by Silly Filly Studios is an awesome animation set before the events of season 1 which has lots of interesting implications for the lore of the MLP universe. It contains spoilers for a couple episodes near the start of season 3 though, so you'll have to wait until then to watch it. Hmm... well actually, you could argue that since the events in this animation are set before season 3, they aren't _really_ spoilers. In fact, watching this animation between season 2 and season 3 might be a good tease/introduction to give a bit of backstory to the events that soon follow. I guess it's really up to you. If you do want to watch it before season 3, I think the best time to do so is after the finale of season 2. In any case, it's an amazing animation.

[**_Snowdrop_**](https://www.youtube.com/watch?v=do6RDSwaWek) by Silly Filly Studios is an awesome fan-created episode set primarily before the events of season 1. It follows the fan-created character Snowdrop as she struggles to find her place in the world. No spoilers as long as you've seen the season 1 premier. ;-)

[**_Double Rainboom_**](https://www.youtube.com/watch?v=FKwldMTmLcY) is a really fun animation created by the Savannah College of Art and Design. The plot is a bit goofy, but it's still an amazing animation filled with lots of laughs, so long as you don't take it too seriously. Possible spoilers for season 2 (if you're being particularly observant), and guarenteed spoilers for season 1 (specifically, the _Sonic Rainboom_ episode).

[**_Button Mash Adventures_**](https://www.youtube.com/watch?v=uFe2MckUAC0) is a great animation created by Jan Animations. No spoilers of any kind, it mostly focuses on a background pony who doesn't have an important role in the real series. There's also a [related video](https://www.youtube.com/watch?v=fWgrmYPplfI) which is a "deleted scene" for an episode in season 2. No spoilers there per-say, but you won't really get the joke until after you've seen the related MLP episode. (There's also the [Bar Buddies](https://www.youtube.com/watch?v=Pi4xYFj-xu4) thing, which is just a short gag. No spoilers.)

[**_Nightmare Night_**](https://www.youtube.com/watch?v=kle9DkkwlM8) is a PMV created by BronyDanceParty based on the song of the same name by WoodenToaster and Mic The Microphone. The animation features the OCs of the song's artists, and the animation quality is seriously awesome. There's also a [Source Film Maker PMV](https://www.youtube.com/watch?v=MRhRaWNaSPM) of the same song made by Ferexes featuring characters from Team Fortress 2. Neither of these animations have spoilers for anything beyond season 2.

[**_Lullaby for a Princess_**](https://www.youtube.com/watch?v=i7PQ9IO-7fU) is an awesome fan-created song by ponyphonic which was recently turned into an epic fully animated musical drama! Approximately 3 years in the making, this video by WarpOut is almost universally considered by the fandom to be one of the greatest fan creations of all time. No spoilers for anything beyond the first 2 minutes of season 1 episode 1. ;-) (It does incorporate some concepts from seasons 2 and 3, but nothing I'd consider significant enough to be a spoiler.) Actually, you could almost just replace the first few minutes of season 1 episode 1 with this video without really disrupting the flow of the story. Just watch it, you'll see what I mean.

[**_Don't Mine at Night_**](https://www.youtube.com/watch?v=X_TMtgjQuZI) is a PMV created by Jan Animations which ponifies the [YOGSCAST parody _Don't Mine at Night_](https://www.youtube.com/watch?v=X_XGxzMrq04), which itself is a Minecraft parody of Katy Perry's song, _Last Friday Night_. No spoilers here whatsoever, but I recommend watching _Button Mash Adventures_ (see above) before watching this, as it will give you a better understanding of one of the characters shown in the animation.

[**_Children of the Night_**](https://www.dailymotion.com/video/x282uju) by Duo Cartoonist is a PMV of the song [_Come Little Children_](https://www.youtube.com/watch?v=1t8-_pI1-9Q) from the movie _Hocus Pocus_. The animation quality is fantastic, and the song fits really well with the underlying story the animation is presenting. In short, it's an awesome PMV. (Not to mention extremely popular, with over 28 million views.) No spoilers.

[**_Remembrance_**](https://www.youtube.com/watch?v=HZDhMKk4tfg) by Argodaemon is an extremely well done 3D animation created with Source Film Maker. The animation quality is amazing, and the story is very touching. You might have a bit of trouble understanding what's happening though unless you're already familiar with a few important bits of [fanon](http://www.urbandictionary.com/define.php?term=fanon), such as (warning, season 3 spoilers in the following link) [the logical consequences](http://oddwarg.deviantart.com/art/Immortality-356563589) of some of the events in season 3. Major spoilers for season 3.

[**_The Moon Rises_**](https://www.youtube.com/watch?v=g2PCNKlddJY) is an epic fan-made song by ponyphonic which now has an equally amazing animation by Duo Cartoonist to go with it! Features a compelling story, great music, and excelent animation quality. No spoilers for anything beyond the first 2 minutes of season 1 episode 1.

[**_Turnabout Storm_**](https://www.youtube.com/watch?v=yUDfoZGhLjE&list=PL347AD9B9E509804A) is probably best described as a visual novel with voice acting. It depicts an epic courtroom drama, in an amazingly well done crossover between MLP and the game franchise [_Phoenix Wright: Ace Attorney_](https://en.wikipedia.org/wiki/Phoenix_Wright:_Ace_Attorney). (I know that sounds stupid, but trust me, it's actually really awesome.) These videos are a lot longer than other fan works (maybe around 9 hours total), but sooooo worth watching, even if you've never played Phoenix Wright. (For maximum enjoyment, I recommend you play through the first couple cases in _Phoenix Wright: Ace Attorney_ at least before watching part 2. Again though, that's not required.) Every commentator I've ever seen watch _Turnabout Storm_ has really enjoyed it. (It even had [xXSoundspeedXx](https://www.youtube.com/user/xXSoundspeedXx) on the verge of tears by the end; his commentary of this series was really great.) Includes no MLP spoilers for anything beyond season 1, but it does incorporate some concepts and humor from season 2 and several fan works. Does include spoilers for the first couple cases of Phoenix Wright, and minor spoilers for Phoenix Wright up to the third game.


### Multi-video Unofficial Series: Luna's Fall

Because of all the various fan-made videos and songs focusing on the events before season 1, especially Luna and her transformation into Nightmare Moon, it turns out it's actually possible to form a sort of coherent story just by watching a few of the more popular videos in the correct order.

Before watching this, you must have at least started season 4 of the show to avoid spoilers.

1. [Harmony Ascendant](https://www.youtube.com/watch?v=82prN2pF9Zo)
2. ["Spare" animation](https://www.youtube.com/watch?v=pepWxrTh1O8)
3. [A Tale of One Shadow](https://www.youtube.com/watch?v=yDNTNXtsN1Y)
4. [Fall of the Crystal Empire](https://www.youtube.com/watch?v=vL4q7BBzanI)
5. [Daylight's End](https://www.youtube.com/watch?v=SLMvyX5yqXI)
6. [The Moon Rises](https://www.youtube.com/watch?v=g2PCNKlddJY)
7. [Lullaby for a Princess](https://www.youtube.com/watch?v=i7PQ9IO-7fU)
8. [Angel of Darkness](https://www.youtube.com/watch?v=cgRkOlLSfgc)

[Here's a YouTube Playlist](https://www.youtube.com/playlist?list=PLjEwdNHiqcMQCBTqNaltpSFzU64f6m6E0) featuring these videos.

## In Closing

So yeah, that's basically it. Again, the above list is by no means complete. I've almost certainly left out a number of great creations, either due to the fact that I'm just unaware of them, because my personal tastes just don't consider them to be all that awesome, or just because I couldn't possibly fit evertyhing in this list. I encourage you to explore the fandom yourself and discover even more super-ultra-extreme-awesome-mazing fan-made content.

I hope you found this guide helpful. If there's anything important I left out that you feel should absolutely be included in the list above, leave a comment below or message me. Bye now. /)
